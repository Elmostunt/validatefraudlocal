package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 */
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class SaleInformation implements Serializable {

    private static final long serialVersionUID = -7975414073737436757L;

    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    private String currentDateTime;
    @Getter
    @Setter
    private Boolean isDisruption;
    @Getter
    @Setter
    private Boolean isPayed;
    @Getter
    @Setter
    private Boolean isIssue;
    @Getter
    @Setter
    private SalesOfficeInformation salesOfficeInformation;
    @Getter
    @Setter
    private ShoppingCartInformation shoppingCartInformation;
    @Getter
    @Setter
    private ExchangeTicketInformation exchangeTicketInformation;

}
