package com.latam.arq.commons.properties;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import lombok.Data;

@Component
@PropertySource("classpath:application.properties")
@Data
public class AppProperties {

    @Value("${ws.tam.url.cybersource}")
    private String urlTamCybersource;

    @Value("${ws.cybersource.tam.reason.codes}")
    public String tamReasonCode;

    @Value("${merchantid.br}")
    public String merchantIdBr;

    @Value("${ws.tam.security.username.br}")
    public String secTamBrUsername;

    @Value("${ws.tam.security.username.int}")
    public String secTamIntUsername;

    @Value("${ws.tam.security.password.base64text.br}")
    public String tamBrCodeB64;

    @Value("${ws.tam.security.password.base64text.int}")
    public String tamIntCodeB64;

    @Value("${ws.tam.endpoint.cybersource}")
    public String endpointTamCybersource;

    @Value("${mechantid.ssc}")
    private String merchantIdSsc;
    
    @Value("${ws.lan.url.cybersource}")
    private String urlLanCybersource;

    @Value("${ws.lan.security.username}")
    private String secLanSSCUsername;

    @Value("${ws.lan.security.password.base64text}")
    private String lanSSCCodeB64;

    @Value("${ws.lan.endpoint.cybersource}")
    private String endpointLanCybersource;

}
