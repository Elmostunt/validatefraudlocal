package com.latam.arq.commons.ws.exception;

public class ParserException extends Exception {

    private static final long serialVersionUID = -6446379435910847470L;

    public ParserException() {
        super();
    }

    public ParserException(String message) {
        super(message);
    }

    public ParserException(Throwable cause) {
        super(cause);
    }

    public ParserException(String message, Throwable cause) {
        super(message, cause);
    }
}
