package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 */
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class ExchangeTicketInformation implements Serializable {

    private static final long serialVersionUID = 2020083788364970329L;

    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    private String issueDate;
    @Getter
    @Setter
    private String ticketNumber;
    @Getter
    @Setter
    private Legs legs;

}
