package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 */
@ToString
@EqualsAndHashCode
public class Items implements Serializable {

    private static final long serialVersionUID = -1782147229191301211L;

    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    private List<Item> item;

    public Items() {
        item = new ArrayList<>();
    }
}
