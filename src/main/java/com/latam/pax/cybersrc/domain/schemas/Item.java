package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * <p>
 * Register of versions:
 * <ul>
 * <li>1.0 05-07-2016, (Everis Chile) - initial release
 * </ul>
 * <p>
 * This class contains the domain object
 * 
 * <p>
 * <B>All rights reserved by Lan.</B>
 */
@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class Item implements Serializable {

    private static final long serialVersionUID = -7265842741559866673L;

    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    private PassengerInformation passengerInformation;
    @Getter
    @Setter
    private String pnrCode;
}
