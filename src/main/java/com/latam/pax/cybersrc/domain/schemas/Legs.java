package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 */
@ToString
@EqualsAndHashCode
public class Legs implements Serializable {

    private static final long serialVersionUID = 8373725748502745225L;

    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    private List<Leg> leg;

    public Legs() {
        leg = new ArrayList<>();
    }

}
