package com.latam.pax.cybersrc.domain.schemas;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@ToString
@EqualsAndHashCode
@NoArgsConstructor
public class DepartureInformation implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -8858423953433825103L;
    
    @NotNull(message = "{domain.not.null}")
    @Getter
    @Setter
    String dateTime;
    @Getter
    @Setter
    String airportIATACode;
    @Getter
    @Setter
    String countryISOCode;

}
