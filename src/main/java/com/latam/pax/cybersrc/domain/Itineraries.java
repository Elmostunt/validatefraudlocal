package com.latam.pax.cybersrc.domain;

import java.io.Serializable;

import javax.validation.constraints.NotNull;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * 
 * 
 * <p>
 * Register of versions:
 * <ul>
 * <li>1.0 11-07-2016, (Everis Chile) - initial release
 * </ul>
 * <p>
 * 
 * 
 * <p>
 * <B>All rights reserved by Lan.</B>
 */
@ToString
@EqualsAndHashCode
@AllArgsConstructor
@NoArgsConstructor
public class Itineraries implements Serializable {

    private static final long serialVersionUID = 1804919252741370258L;

    @NotNull(message = "{not.null.attribute}")
    @Getter
    @Setter
    private String itinerary = "";
    @Getter
    @Setter
    private String itineraryCountry = "";
    @Getter
    @Setter
    private String countryInitial = "";
    @Getter
    @Setter
    private String itineraryAirline = "";
    @Getter
    @Setter
    private String itineraryClass = "";
}
