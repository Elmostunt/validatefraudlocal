package com.latam.pax.cybersrc.domain.type;

public enum DateType {
    YEAR("YEAR", (1000 * 60 * 60 * 24 * 365)), DAY("DAY", (1000 * 60 * 60 * 24)), HOUR("HOUR", (1000 * 60 * 60)),
    MINUTE("MINUTE", (1000 * 60)), SECOND("SECOND", (1000));

    private final String label;
    private final int factor;

    DateType(String label, int value) {
        this.label = label;
        this.factor = value;
    }

    public String getLabel() {
        return label;
    }

    public int getFactor() {
        return factor;
    }

    public static DateType fromValue(String pValue) {

        for (DateType selection : DateType.values()) {
            if (pValue.equals(selection.label)) {
                return selection;
            }
        }
        return YEAR;
    }
}
