package com.latam.pax.cybersrc.br.utils;

import com.latam.pax.cybersrc.commons.GlobalConstant;
import com.latam.pax.cybersrc.domain.Itineraries;
import com.latam.pax.cybersrc.domain.schemas.Leg;
import com.latam.pax.cybersrc.domain.schemas.ValidateFraudRQ;
import com.latam.ws.ics.icstransaction.BillTo;
import com.latam.ws.ics.icstransaction.DecisionManager;
import com.latam.ws.ics.icstransaction.DecisionManagerTravelData;
import com.latam.ws.ics.icstransaction.ObjectFactory;
import com.latam.ws.ics.icstransaction.RequestMessage;

public class ParserRequestBRAux {
    
    protected ParserRequestBRAux() {
        
    }
    public static Itineraries getItinerary(ValidateFraudRQ request) {
        Itineraries itinerary = new Itineraries();
        for (Leg leg : request.getLegs().getLeg()) {
            if (!"".equals(itinerary.getItinerary())) {
                itinerary.setItinerary(itinerary.getItinerary() + GlobalConstant.STR_TWO_POINT);
                itinerary.setItineraryCountry(itinerary.getItineraryCountry() + GlobalConstant.STR_TWO_POINT);
                itinerary.setItineraryAirline(itinerary.getItineraryAirline() + GlobalConstant.STR_GUION);
                itinerary.setItineraryClass(itinerary.getItineraryClass() + GlobalConstant.STR_GUION);
            } else {
                itinerary.setCountryInitial(leg.getDepartureInformation().getCountryISOCode());
            }
            itinerary.setItinerary(itinerary.getItinerary() + leg.getDepartureInformation().getAirportIATACode()
                    + GlobalConstant.STR_GUION + leg.getArrivalInformation().getAirportIATACode());
            itinerary.setItineraryCountry(
                    itinerary.getItineraryCountry() + leg.getDepartureInformation().getCountryISOCode()
                            + GlobalConstant.STR_GUION + leg.getArrivalInformation().getCountryISOCode());
            itinerary.setItineraryAirline(itinerary.getItineraryAirline() + leg.getAirlineIATACode());
            itinerary.setItineraryClass(itinerary.getItineraryClass() + leg.getFareClass());
        }
        return itinerary;
    }

    public static void fillDecisionManager(ValidateFraudRQ request, ObjectFactory factoryCybersource,
            RequestMessage requestCybersource) {
        // G.Cárcamo - Dado que el tramo se saca de los legs, se valida que no esten
        // nulos
        if (!request.getLegs().getLeg().isEmpty()) {

            DecisionManager decisionManager = factoryCybersource.createDecisionManager();
            DecisionManagerTravelData travelData = factoryCybersource.createDecisionManagerTravelData();

            // G.Carcamo - Fecha del primer tramo--------------------------------
            travelData.setDepartureDateTime(
                    request.getLegs().getLeg().get(0).getDepartureInformation().getDateTime() + "GMT");

            // -------------------Complete Route---------------------------------
            StringBuilder completeRoute = new StringBuilder();
            for (Leg legType : request.getLegs().getLeg()) {
                if (!completeRoute.toString().isEmpty()) {
                    completeRoute.append(GlobalConstant.STR_TWO_POINT);
                }
                completeRoute.append(legType.getDepartureInformation().getAirportIATACode() + GlobalConstant.STR_GUION
                        + legType.getArrivalInformation().getAirportIATACode());
            }
            travelData.setCompleteRoute(completeRoute.toString());
            decisionManager.setTravelData(travelData);
            requestCybersource.setDecisionManager(decisionManager);
            // --------------------------------------------------------------------
        }
    }
    
    public static void fillBillTo(ValidateFraudRQ body,
            com.latam.ws.ics.icstransaction.ObjectFactory factoryCybersource, RequestMessage requestCybersource) {
        BillTo billTo = factoryCybersource.createBillTo();
        // PNR-Code Revisar campo dado cambio de schema
        billTo.setCustomerID(
                body.getSaleInformation().getShoppingCartInformation().getItems().getItem().get(0).getPnrCode());
        requestCybersource.setBillTo(billTo);
    }
}
