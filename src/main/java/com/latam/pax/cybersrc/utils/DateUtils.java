package com.latam.pax.cybersrc.utils;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

import com.latam.arq.commons.ws.exception.ParserException;
import com.latam.pax.cybersrc.domain.type.DateType;

public class DateUtils {

    public static final String DATE_FORMAT_LEGS = "yyyy-MM-dd HH:mm";

    public static String getDateServerString() {
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd-MM-yyyy");
        return LocalDate.now().format(formatter);

    }

    public LocalDateTime transformDateTime(String dateTimeString, String format) throws ParserException {
        try {
            LocalDateTime localDateTime = LocalDateTime.now();
            if (null != dateTimeString && null != format) {
                localDateTime = LocalDateTime.parse(dateTimeString, DateTimeFormatter.ofPattern(format));
            }
            return localDateTime;

        } catch (DateTimeParseException e) {
            PrintUtils.printErrorData(e.getMessage() + e);
            throw new ParserException(e.getMessage(), null);
        }
    }

    public LocalDateTime getDateServer() {
        return LocalDateTime.now();
    }

    public long getDiference(LocalDateTime dateInitial, LocalDateTime dateEnd, DateType dateType) throws ParserException{
        try {  
        PrintUtils.printInfoData("Fecha inicial " + dateInitial + "Fecha final " + dateEnd);
            long dife = Timestamp.valueOf(dateEnd).getTime() - Timestamp.valueOf(dateInitial).getTime();
            return (dife / dateType.getFactor());
        }catch(DateTimeParseException e){
            PrintUtils.printErrorData(e.getMessage() + e);
            throw new ParserException(e.getMessage(), null);
        }
    }

}
