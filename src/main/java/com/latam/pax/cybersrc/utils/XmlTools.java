package com.latam.pax.cybersrc.utils;

import com.thoughtworks.xstream.XStream;
import com.thoughtworks.xstream.io.xml.DomDriver;

/**
 * 
 * @author equipo Allegro everis -LATAM
 *
 */
public final class XmlTools {

    private static XStream stream = new XStream();

    private XmlTools() {

    }

    static {
        stream.setMode(XStream.NO_REFERENCES);
    }

    public static String toXml(Object object, boolean ajax) {
        if (object != null) {
            String xml = stream.toXML(object);
            if (ajax) {
                xml = xml.replaceAll("com.latam.pax.allegro.domain.mgr.", "");
            }
            return xml;
        }
        return null;
    }

    public static String toXml(Object object) {
        return toXml(object, false);
    }

    public static Object toObjectFromXML(String xml) {
        XStream stream = new XStream(new DomDriver());
        return stream.fromXML(xml);
    }
}
